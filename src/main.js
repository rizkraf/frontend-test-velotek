import { createApp } from "vue";
import App from "./App.vue";
import "./assets/styles/index.css";
import "animate.css";

const app = createApp(App);

app.mount("#app");
