/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        primary: ["Poppins", "sans-serif"],
      },
      colors: {
        black: "#212529",
        white: "#F8F9FA",
        gray: "#6C757D",
        primary: "#3D5A80",
        secondary: "#EE6C4D",
        success: "#8AC926",
        danger: "#EF233C",
        warning: "#FAA307",
        purple: "#5E548E",
        modal: "rgba(33, 37, 41, 0.7)",
        "light-red": "#EE6C4D",
        "light-blue": "#00AFB9",
        "dark-blue": "#006D77",
        "pale-blue": "#83A9BF",
      },
      boxShadow: {
        button: "0px 4px 10px rgba(0, 0, 0, 0.25)",
        'button-table': '0px 4px 10px rgba(0, 0, 0, 0.1)',
        card: "0px 4px 10px rgba(0, 0, 0, 0.05)",
      },
      borderRadius: {
        card: "15px",
      },
      width: {
        18: "70px",
      },
    },
  },
  plugins: [],
};
